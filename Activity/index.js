/*
    Activity:

    1. Update and Debug the following codes to ES6.
        Use template literals,
        Use array/object destructuring,
        Use arrow functions
	
    2. Create a class constructor able to receive 3 arguments
        It should be able to receive 3 strings and a number
        Using the this keyword assign properties:
        username, 
        role, 
        guildName,
        level 
  assign the parameters as values to each property.
  Create 2 new objects using our class constructor.
  This constructor should be able to create Character objects.



    Create 2 new objects using our class constructor.

    This constructor should be able to create Dog objects.

    Log the 2 new Dog objects in the console.


    Pushing Instructions:

    Create a git repository named S24.
    Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
    Add the link in Boodle.

*/

//Solution: 

/*Debug*/
let student1 = {
    name: "Shawn Michaels",
    birthday: "May 5, 2003",
    age: 18,
    isEnrolled: true,
    classes: ["Philosphy 101", "Social Sciences 201"]
}


let student2 = {
    name: "Steve Austin",
    birthday: "June 15, 2001",
    age: 20,
    isEnrolled: true,
    classes: ["Philosphy 401", "Natural Sciences 402"]
}


/*Debug and Update*/

function introduce(student) {

    console.log(`Hi! I'm ${student.name}. I am ${student.age} years old.`)
    console.log(`I study the following courses:  ${student.classes}`)

}

introduce(student1);
introduce(student2);

let getCube = (num) => {

    console.log(num ** 3);

}
getCube(15);
let numArr = [15, 16, 32, 21, 21, 2]


numArr.forEach((num) => {
    console.log(num)
  });

let numsSquared = numArr.map((num) => {
    return num**2;
  });

console.log(numsSquared);


/*2. Class Constructor*/

function character(name, role, guild, level) {

    this.username = name;
    this.role = role;
    this.guildName = guild;
    this.level = level;
}

let character1 = new character("Sung Jinwoo", "Guild Master", "Ahjin Guild", "100");
console.log(character1);

let character2 = new character("Cha Hae-In", "Vice-Guild Master", "Hunters Guild", "80");
console.log(character2);

let character3 = new character("Sung Il-Hwan", "???", "???", "???");
console.log(character3);

let character4= new character("Thomas Andre", "Guild Master", "Scavenger Guild", "85");
console.log(character4);

